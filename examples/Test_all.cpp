#include <Arduino.h>
#include "Epaper.h"

//STM32 bluepill test example
Epaper display = Epaper(A4,A3,A2,A1);

void setup() {
  Serial.begin(115200);
  Serial.println("Starting test");

  Serial.print("Init display ...");
  display.init();
  Serial.println("Done");

  Serial.print("Display center x: ");
  Serial.println(String(display.get_center_x()));

  Serial.print("Display center y: ");
  Serial.println(String(display.get_center_y()));

  Serial.print("Display width: ");
  Serial.println(String(display.get_width()));

  Serial.print("Display height: ");
  Serial.println(String(display.get_height()));


}

void loop() {
  
  Serial.print("Printing test ...");
  for (int i = 0; i < 100; i+=10)
  {
      display.print_partial(i,i+20,"Hello world!");
      delay(1000);
      display.clear_rect(i,i+20,10,60);
      delay(1000);

  }
  Serial.println("Done");
  
  delay(2000);
  
  Serial.print("Clear display ...");
  display.clear();
  Serial.println("Done");
  
  delay(1000);
}