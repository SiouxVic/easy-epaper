
#ifndef EPAPER
#define EPAPER

#include "Arduino.h"
#include "Adafruit_I2CDevice.h"
#include <GxEPD2_BW.h>
#include <Fonts/FreeMonoBold9pt7b.h>

#define MAX_DISPLAY_BUFFER_SIZE 800
#define MAX_HEIGHT(EPD) (EPD::HEIGHT <= MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8) ? EPD::HEIGHT : MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8))

struct boundaries {
    int16_t x;
    int16_t y;
    uint16_t width;
    uint16_t height;
};

class Epaper {
private:
  GxEPD2_BW<GxEPD2_290, MAX_HEIGHT(GxEPD2_290)> *display;

public:
  Epaper (int cs,int dc,int rst,int busy);
  void print_partial(int16_t tbx,int16_t tby,String value, float y_offset_ratio, bool invert_color = false);
  void print_partial(int16_t tbx,int16_t tby,String value, bool invert_color = false);
  boundaries getTextBounds(const String &str, int16_t x, int16_t y);
  void init();
  void clear();
  void clear_rect(boundaries rect);
  void clear_rect(int x, int y, int width, int height);
  void fill_rect(boundaries rect);
  void fill_rect(int x, int y, int width, int height);
  void set_font(const GFXfont* font);
  int get_height();
  int get_width();
  int get_center_y();
  int get_center_x();

};

#endif /* end of include guard: Epaper */
