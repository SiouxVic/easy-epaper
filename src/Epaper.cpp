#include "Epaper.h"

//Taken from the example code from the GxEPD2 library

Epaper::Epaper(int cs,int dc,int rst,int busy){
  display = new GxEPD2_BW<GxEPD2_290, MAX_HEIGHT(GxEPD2_290)>(GxEPD2_290(/*CS=*/cs, /*DC=*/ dc, /*RST=*/ rst, /*BUSY=*/ busy));
}

int Epaper::get_center_x(){
  return (display->width() / 2);
}
int Epaper::get_center_y(){
  return (display->height() / 2);
}
int Epaper::get_width(){
  return display->width();
}
int Epaper::get_height(){
  return display->height();
}
void Epaper::init(){
  display->init();
  display->setFont(&FreeMonoBold9pt7b);
  display->setRotation(1);
  display->setTextColor(GxEPD_BLACK);
  clear();
}
void Epaper::set_font(const GFXfont* font){
  display->setFont(font);
}
//Antiburn
void Epaper::clear(){
  display->setFullWindow();
  display->firstPage();
  do{
    display->fillScreen(GxEPD_WHITE);
  }while (display->nextPage());

}

void Epaper::clear_rect(boundaries rect){
  clear_rect(rect.x,rect.y,rect.width,rect.height);
}

void Epaper::clear_rect(int x, int y, int width, int height){
  display->setPartialWindow(x, y, width, height);
  display->firstPage();
  do{
    display->fillRect(x, y, width, height, GxEPD_WHITE);
  }while (display->nextPage());
}

void Epaper::fill_rect(boundaries rect){
  fill_rect(rect.x,rect.y,rect.width,rect.height);
}

void Epaper::fill_rect(int x, int y, int width, int height){
  display->setPartialWindow(x, y, width, height);
  display->firstPage();
  do{
    display->fillRect(x, y, width, height, GxEPD_BLACK);
  }while (display->nextPage());
}

boundaries Epaper::getTextBounds(const String &str, int16_t x, int16_t y){
  boundaries b;
  int16_t
    x1 = 0,
    y1 = 0;
  uint16_t
    w = 0,
    h = 0;

  display->getTextBounds(str, x, y, &x1, &y1, &w, &h);

  b.x = x;
  b.y = y1 + (h * 0.75f);
  b.width = w + (x1 - x);
  b.height = h;
  display->setTextColor(GxEPD_BLACK);

  return (b);
}

void Epaper::print_partial(int16_t tbx,int16_t tby,String value, float y_offset_ratio, bool invert_color){
  boundaries txtBounds = getTextBounds(value, tbx, tby);
  display->setPartialWindow(txtBounds.x, txtBounds.y, txtBounds.width, txtBounds.height);
  display->firstPage();
  do{
    if (invert_color) {
      display->setTextColor(GxEPD_WHITE);
      display->fillRect(txtBounds.x, txtBounds.y, txtBounds.width, txtBounds.height, GxEPD_BLACK);
    }else{
      display->setTextColor(GxEPD_BLACK);
      display->fillRect(txtBounds.x, txtBounds.y, txtBounds.width, txtBounds.height, GxEPD_WHITE);
    }
    display->setCursor(txtBounds.x, txtBounds.y + (txtBounds.height * y_offset_ratio));
    display->print(value);
  }while (display->nextPage());
}
void Epaper::print_partial(int16_t tbx,int16_t tby,String value, bool invert_color){
  print_partial(tbx, tby, value, 0.75f,invert_color);
}
